const defaultAppSettings = {
    loading: false,
    appColor: '#BADA55'
};

const storeItem = (itemType, item) => {
    localStorage.setItem(itemType, JSON.stringify(item));
};

const getItem = itemType => JSON.parse(localStorage.getItem(itemType));

const imitateApiDelay = contentToServe => new Promise((resolve) => {
    setTimeout(() => resolve(contentToServe), 1000);
});

export default {
    updateAppSettings(appSettings) {
        storeItem('appSettings', appSettings);
        return imitateApiDelay(appSettings);
    },

    getAppSettings() {
        const appSettings = getItem('appSettings') || defaultAppSettings;
        return imitateApiDelay(appSettings);
    },

    getTasks() {
        return fetch('http://localhost:4592/api/todos')
            .then(function (response) {
                return response.json(); 
            })        
    },

    getAppTodoById(id) {
        //const appTodos = getItem('appTodos') || [];

        //const todoById = appTodos.find(todo => todo.id === id);

        //return imitateApiDelay({
        //    success: true,
        //    todo: todoById
        //});

        return fetch('http://localhost:4592/api/todos/' + id)
            .then(function (response) {
                return response.json();
            })
            .then(function (responseObj) {
                return {
                    success: true,
                    todo: responseObj
                };
            })
            .catch(function () {
                return {
                    success: false,
                    todo: { id: id, title: 'error', completed: false }
                };
            });
    },

    addAppTodo(title) {          
        //id cannot start with a number, su add a letter at the beginning 
        const newId = 'tdi-' + Math.random().toString(36).substring(2, 15);
        const newTodo =
            {
                id: newId,
                title,
                completed: false
            };


        return fetch('http://localhost:4592/api/todos', {
            method: 'post',
            body: JSON.stringify(newTodo),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
            .then(function (response) {
                return newId;
            });   
    },

    removeAppTodo(id) {
        return fetch('http://localhost:4592/api/todos/' + id, {
            method: 'delete',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
        .then(function (response) {
            return {
                success: true,
            };
        });  
    },

    toggleAppTodo(id) {
        const appTodos = getItem('appTodos') || [];

        const toggledTodos = appTodos.map((todo) => {
            if (todo.id === id) {
                return {
                    ...todo,
                    completed: !todo.completed
                };
            }
            return todo;
        });

        storeItem('appTodos', toggledTodos);

        return imitateApiDelay({ success: true });
    }
};


/*
 1. Send trivial, get trivial
 2. Send id, get model
 3. Send nothing, get array of model

 https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

 */