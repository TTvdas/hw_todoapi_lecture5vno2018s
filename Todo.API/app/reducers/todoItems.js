export default (state = { items: [], loading: false }, action) => {
    switch (action.type) {
        case 'SET_LOADING':
            return {
                ...state,
                loading: true
            };
        case 'LOAD_ALL':
            return {
                ...state,
                loading: false,
                items: action.items
            };
        case 'ADD_NEW':
            return {
                ...state,
                loading: false,
                items: [
                    ...state.items,
                    action.todo
                ]
            };
        case 'DELETE_ITEM':
            let index = state.items.findIndex(todoItem => todoItem.id === action.itemId);
            return {
                ...state,
                items: [
                    ...state.items.slice(0, index),
                    ...state.items.slice(index + 1)
                ]
            };
        default:
            return state;
    }
};
