﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Todo.API.Controllers
{
    [Route("api/todos")]
    public class TodosController  : Controller
    {
        private static List<Todo> _todos = new List<Todo>
            {
              new Todo {id = "id1", title = "learn js"},
              new Todo { id = "idLinas", title = "never touch js", completed = true}

            };

        [HttpGet]
        public IActionResult GetAll([FromQuery(Name = "name")] string name, [FromQuery(Name = "completed")] bool isCompleted, 
            [FromQuery(Name ="sortDirection")] string sortDirection)
        {
            var queryResult = _todos;

            if(Request.Query.Keys.Any(key => key == "name"))
            {
                queryResult = queryResult.FindAll(todo => todo.title == name);
            }

            if(Request.Query.Keys.Any(key => key == "completed")){
                queryResult = queryResult.FindAll(todo => todo.completed == isCompleted);
            }

            if(Request.Query.Keys.Any(key => key == "sortDirection"))
            {
                if (sortDirection.Equals("asc"))
                {
                    queryResult = queryResult.OrderBy(todo => todo.title).ToList();
                }
                else if (sortDirection.Equals("desc"))
                {
                    queryResult = queryResult.OrderByDescending(todo => todo.title).ToList();
                }
                else
                {
                    return BadRequest("The sortDirection values can only be asc and desc");
                }
            }

            return Ok(queryResult.ToList());
        }

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            if (String.IsNullOrWhiteSpace(id)) { return NoContent(); }

            var result = _todos.FirstOrDefault(todo => todo.id == id);
            if(result != null)
            {
                return Ok(result);
            }
            return NoContent();
        }

        [HttpPost, HttpPut]
        public IActionResult Post([FromBody]Todo todo)
        {
            // Validate
            if(todo == null) { return BadRequest("Can't insert null todo"); }
            if (String.IsNullOrWhiteSpace(todo.title)){ return BadRequest("Can't insert todo with empty title"); }


            // Init id if necessary
            if (todo.id == null) { todo.id = Guid.NewGuid().ToString(); }


            // Check if todo with such id already exists
            var existingTodo = _todos.FirstOrDefault(elem => elem.id == todo.id);


            // If there is not such todo - insert
            if(existingTodo == null)
            {
                _todos.Add(todo);
            }
            else 
            {
                _todos.Remove(existingTodo);
                _todos.Add(todo);
            }
            
            return Ok(todo.id);
        }

       


        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var todoToDElete = _todos.FirstOrDefault(todo => todo.id == id);
            if(todoToDElete == null) { return NotFound($"Can't remove todo with id {id}"); }

            _todos.Remove(todoToDElete);
            return Ok();
        }
    }

    public class Todo
    {
        public string id { get; set; }
        public string title { get; set; }
        public bool completed { get; set; }
    }
}
