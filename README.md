HW_TodoAPI_lecture5vno2018s

Update GET /api/todos endpoint with a query parameter that filters todos by name (host/api/todos?name=”Go To Bar”).

Update GET /api/todos endpoint with a completed boolean parameters that filters items by completed status (host/api/todos?completed=false).

Update GET /api/todos endpoint with a sortDirection parameter that, if specified, sorts todos by name.